# Vortexus

A multi-purpose Discord bot written in discord.js designed to help server owners moderate and manage their server.


## Links

* [Documentation](https://gitlab.com/OfficialInfi/vortexus-rewrite/-/wikis/home)


## Information

* [Contributing Guidelines](https://officialinfi.gitlab.io/pages/ProjectContributing)

* [Code of Conduct](https://officialinfi.gitlab.io/pages/ProjectCodeofConduct)


## Support

* [Discord Server](https://discord.gg/qBcwXD2)

* [Subreddit](https://reddit.com/r/unknowncommunity)


## Acknowledgements

You can view the people who have influenced this project [here](https://gitlab.com/OfficialInfi/vortexus-rewrite/-/wikis/Acknowledgements). You can also view people who have contributed to the repository [here](https://gitlab.com/OfficialInfi/vortexus-rewrite/-/graphs/master).

This project is licensed under the [MIT License](https://officialinfi.gitlab.io/pages/ProjectLicense).