# Configuration

In order to use Vortexus, you must configure it to your liking. This page will show you how to configure your bot.

## Bot Configuration

`config/bot.json` is used for global configuration, such as the token used to log in.

| Key | Description | Type | Required |
| --- | --- | --- | --- |
| token | The bot token used to log in. | `"String"` |**Yes** |
| command_cooldown | A global cooldown *(in seconds)* for commands. | `Number` | **Yes** |
| prefix | The prefix that is used for all commands. | `"String"` | **Yes**
| game | The status that is shown next to your bot's username. | `"String"` | **Yes**

### Example

```json
    {
    "token": "Th1s1SN0TaR341T0K3N",
    "command_cooldown": 3,
    "prefix": "!",
    "game": "Use !help",

    "modules": {
        "core": {
            "help": true,
            "stop": true,
            "eval": true,
            "stats": true
        }
    }
}
```

## Server Configuration

Each server has it's own configuration file. To start configuring your server, go to `/config/` and create a new file: `ID.guild.json`. Make sure to replace `ID` with your own server's ID.

| Key | Description | Type | Required |
| --- | --- | --- | --- |
| command_cooldown | A server cooldown *(in seconds)* for commands. | `Number` | **No** |
| modules | An object containing all modules. | `{"Type":"Object"}` | **Yes** |

## Modules

The `modules` key contains all modules for commands.

### Moderation

The moderation module is used for moderating your server.

| Key | Description | Example |
| --- | --- | --- |
| kick | Kick command | `{"enabled": true/false,"reasonRequired": true/false}` |
| ban | Ban command | `{"enabled": true/false,"reasonRequired": true/false}` |
| mute | Mute command | `{"enabled": true/false,"reasonRequired": true/false, "addRole": RoleID}` |
| unmute | Unmute command | `{"enabled": true/false,"reasonRequired": true/false, "removeRole": RoleID}` |

### Fun

The fun module is used for fun commands.

| Key | Description | Example |
| --- | --- | --- |
| starboard | Starboard | `{"enabled": true/false,"channel": ChannelID, "minimum": number, "selfstar": true/false}` |
| levels | Level system | `{"enabled": true, "requiredEXP": "10"}` |

### Economy

The economy module is used for economy commands.

| Key | Description | Example |
| --- | --- | --- |
| work | Work command | `{"enabled": true,"max": "15"}` |

## Example

The following example defines the "moderation" module with a "kick" command. The kick command is enabled, and requires a reason. A command is enabled by setting the command value to `true` or defining the command value as an object, and having a key in that object called `enabled` to be `true`.

```json
"modules": {
    "moderation": {
        "kick": {
            "enabled": true,

            "reasonRequired": true
        },
    }
}
```