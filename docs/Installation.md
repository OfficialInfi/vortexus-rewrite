# Installation

This page will explain how to install and setup Vortexus.

## Step 1: Install Dependencies

### Node.JS

Vortexus uses Node.JS, which can be installed on nearly all platforms.

* **Windows & MacOS** - [nodejs.org](https://nodejs.org/en/download/)

* **Linux** - Check your distribution's package manager, or get it from [nodejs.org](https://nodejs.org/en/download/)

### NPM

Vortexus needs modules (mainly discord.js) to function properly. NPM is a module distributor for Node.JS modules that should come installed with Node. If not, you can get it from [npmjs.com](https://www.npmjs.com/get-npm)

You can install a module by going to the directory where Vortexus is located (usually where the `index.js` file is) and running `npm install {moduleName}`.

Vortexus needs the following modules:

* **discord.js/discord.js** - Connect to Discord. **Make sure your on master/v12**
* **bufferutil** - Speed up websocket connections.
* **erlpack** - Faster websocket (de)serialisation.
* **request** - Make web requests
* **keyv** - Databases.
* **@keyv/sqlite** - Also databases.

## Step 2: Install Vortexus

You can go to the [releases page](https://gitlab.com/OfficialInfi/vortexus-rewrite/-/releases) to install the latest Vortexus release. Make sure to download the .zip file.

When you download the .zip file, unzip it.