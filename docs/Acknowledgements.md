# Acknowledgements

* [discord.js](https://discord.js.org) - Library

* [discord.js Guide](https://discordjs.guide) - Documentation & Tutorials

* [discord.js Discord](https://discord.gg/bRCvFy9) - Support

* [Keyv](https://github.com/lukechilds/keyv) - Database

* [Google's Material Icons](https://material.io/resources/icons/) - Icons