# Commands

This wiki page will list and explain all commands included in Vortexus.

## Core

| Command | Description | Permissions | Arguments |
| --- | --- | --- | --- |
| help | Sends a DM to the user with a help command, or explains a command. | None | `[command]` |
| stop | Shuts down the bot. | TRUSTED | None |
| eval | Evaluate plain Javascript code inside a sandbox, no node.js variables can be accessed except for `msg`, which points to the author's message. | TRUSTED | `{code}` |
| stats | View statistics related to the bot | None | None |

## Moderation

| Command | Description | Permissions | Arguments |
| --- | --- | --- | --- |
| kick | Kicks a user from the current guild. | KICK_MEMBERS | `{user mention/id}` |
| ban | Bans a user from the current guild. | BAN_MEMBERS | `{user mention/id}` |
| mute | Gives a user a specific role. | MANAGE_MESSAGES | `{user mention/id}` |
| unban | Unbans a user from the current guild. | BAN_MEMBERS | `{user mention/id}`
| unmute | Removes a specific role from a user. | MANAGE_MESSAGES | `{user mention/id}` |
| purge | Remove up to 100 messages from a channel that are less than two weeks old. | MANAGE_MESSAGES | `{channel} {number of messages}`

## Fun

| Command | Description | Permissions | Arguments |
| --- | --- | --- | --- |
| rank | Get your level and EXP | None | `{user mention/id}` |
| meme | Get a random post from the hot page of r/dankmemes | None | None |
| reddit | Get a random post from a subreddit. | None | `{subreddit without the r/}` |
| topic | Get a random question from r/askreddit | None | None |
| xkcd | Get a random xkcd comic from 1-2220 | None | `[number inbetween 1-2220]`



## Economy

| Command | Description | Permissions | Arguments |
| --- | --- | --- | --- |
| balance | Shows a user's balance. | None | `[user mention/id]`
| economy | Manage a user's balance. | MANAGE_SERVER | `{set/add/del} {user mention/id} {value}` |
| give | Give money to someone | None | `{user mention/id} {amount}` |
| gamble | Guess a random number inbetween 1-10, if you are right, you win $10, if you are wrong you lose $5 | None | `{guess}`
| work | Work for money. | None | None |


## Utility

| Command | Description | Permissions | Arguments |
| --- | --- | --- | --- |
| guild | Shows information about the current server. | None | `None` |
| tag | Retrieves a tag for the current server. | None | `{tag name}` |
| mtag | Sets or deletes tags for the current server. | MANAGE_MESSAGES | `{set/del} {tag name} [tag content]` |
| suggest | Adds a suggestion to the server. | None | `{suggestion}` |
| avatar | Shows a user's avatar | None | `[user mention/id]` |
| user | Get information on a user | None | `[user mention/id]` |
| poll | Create a reaction poll. | None | None |
| rng | Generate a random number between min-max | None | `{max} [min]` |
