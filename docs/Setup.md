# Setup

Once you've installed Vortexus, you need to setup the bot. This page will explain how to do that.

## Configuration

Vortexus has two main configuration files. **bot.json** and **{guildID}.guild.json**, which are both located in `/config`.

`template-bot.json` is a template for `bot.json`, and `template-guild.json` is a template for `{guildID}.guild.json`.

`bot.json` is used for general values, like the bot's token. [This wiki page](Configuration) explains how to do this.

`{guildID}.guild.json` is used for specific guild values, like if the !suggest command is enabled or disabled. There can be multiple of these files, with the {guildID} in the name being the guild's specific ID. [This wiki page](Configuration) explains how to do this.

## Usage

Once you've configured Vortexus to your liking, you can begin to use it. Go to the directory where you installed Vortexus, open your Terminal/Command Prompt, and run `node index.js`.

If you ever want to stop the bot, just run the `stop` command.