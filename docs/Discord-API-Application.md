# Discord API Application

This page will discuss how to create a Discord Application for your bot.


## Creating an application

Head over to [Discord's Developer Hub](https://discordapp.com/developers/applications) and create yourself an application.

Enter your application's name under "Name". Avatar and Description can be ignored for now. Now save changes.

## Creating a Bot Account

Navigate over to the *"Bot" section and press *"Add Bot"* and accept the changes.

You will see *"Click to Reveal Token"* in Bot Information Section, your Token is sensitive information and should NEVER be shared. If your token does get into somebody's hands who shouldn't have it click the: Regenerate button, this will invalidate all prior tokens.

Now you should have a token which you can use for Vortexus, save this token for later.

## Reminder

Your token is **sensitive information** and should **NEVER** be shared! Anyone with access to your token can take over your bot's account.