# Integrity

This file is intended to confirm the integrity of files to ensure that they haven't been tampered with.

Compare the hash with the hash of the downloaded .ZIP file for proper results.

This file may have been tampered, so ensure you are downloading it from a proper source. Using HTTPs is recommended.

## Release v1.4.0.0 .zip

* **MD5** - `d4cf7d832b473044578037ac9f999458`

* **SHA1** - `ee2191d7c6185f3a6efc3a7cce9cb18f644b19a0`

* **SHA256** - `2d735d000fc806f37e3b282a4699a0376d0e292f6e060f3c389fcfc07bd2bf16`

* **SHA512** - `4ce2df6784f4d03f0c45a0402553bdb55c453ab1862a9a5867494054107658d923aba15d439b43350645ccae4e357597e6a8ab8c6dd3de78f02507584e054578`

## Release v1.3.0.0 .zip

* **MD5** - `01579ee272ce6261b4e8a419ab1ac278`

* **SHA1** - `5a8a8d9af7fd4c5877d0268637b0ed794bb09ba4`

* **SHA256** - `8f580033d43ab85b8c0a55289ff75a367dbbe4dbb67f87dd375ba403055caac7`

* **SHA512** - `469f142f8dcaa41d29160624435639cbc2f6c9ccb57f563a2fc6de8d559921746da584fed8ca077582ab116c0d308896b7e5c223b799bcd03f8b5a89fded251d`

## Release v1.2.0.0 .zip

* **MD5** - `2942cac70348de932e1c31ac5ce6a248`

* **SHA1** - `bb0bcfcee79ee63b9e768370e04abd40dc109b3b`

* **SHA256** - `068a3f75442051ffce5519f78b80b6eb130afc80df2eeb3edf6b2fd890313932`

* **SHA512** - `d3c53b9b4877f047cb09a0c0ef7beaf437b3464f895087969b6a779e2e11a45a32fcf8b9a12b565cd071910e7f5184e566916b8feeb7662841d666c820dc1f39`

## Release v1.1.0.0 .zip

* **MD5** - `66ea9fbc7cbce7478fc11a6e34e9b5f2`

* **SHA1** - `9d5db3d2d0565351aa16f3cb44bf23f3a0b32c71`

* **SHA256** - `df976b4dcbd74506b3d89497e063cf5288c03a3baaed5d3c4e2459cc363ac4d7`

* **SHA512** - `90cab761842cb09e5b5fd023e60f506263e87f1087f27cfd7987acb44d2d473150e355fe68d6e2b54f4ab7e0baf604ab8aa19df2714cd84a4cbfa1fa6fa22ec2`

## Release v1.0.0.0 .zip

* **MD5** - `0e9bf00d38d21670c54a8cea367abf35`

* **SHA1** - `791267bf54b1bb4dba9d8a6f78e62b3a8ab7d65d`

* **SHA256** - `8e380a4a1cdd7af610bd82d370dd146c6126ffd0e800dbc2f652c9f0ea43da37`

* **SHA512** - `93b8bb52c9c970168a0a7e84ae3bd342a41d27a0b34c2718f6c3a7ef5eb84ebb17103d649e99030039beddb7bccc6cd74a9e588123effe39f4c67638686f55ef`

