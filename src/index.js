var Bot = {
    Version: "1.4.0.0",
    Discord: require("discord.js"),

    Helper: {
        FS: require("fs"),
        VM: require("vm"),
        bufferUtil: require("bufferutil"),
        erlpack: require("erlpack"),
        Request: require("request"),

        Logger: require("./bot/helper/Logger.js"),
        Database: require("./bot/helper/Database.js"),
        Embed: require("./bot/helper/Embed.js"),
        Time: require("./bot/helper/Time.js"),
        Automod: require("./bot/helper/Automod.js")
    },
    Config: {
        Bot: require("./config/bot.json")
    },
    Events: {
        Ready: require("./bot/events/ready.js"),
        Message: require("./bot/events/message.js"),
        MessageDelete: require("./bot/events/messageDelete.js"),
        MessageUpdate: require("./bot/events/messageUpdate.js"),
        messageReactionAdd: require("./bot/events/messageReactionAdd.js"),
        guildMemberUpdate: require("./bot/events/guildMemberUpdate.js")
    }
}
Bot.Client = new Bot.Discord.Client({partials:["MESSAGE"]})
Bot.Commands = new Bot.Discord.Collection()
Bot.Cooldowns = new Bot.Discord.Collection()
const GuildConfig = Bot.Helper.FS.readdirSync("./config").filter(file => file.endsWith(".guild.json"))
const commandFiles = Bot.Helper.FS.readdirSync('./bot/commands').filter(file => file.endsWith('.js'))
for (const file of GuildConfig) {
    const File = require("./config/"+file)
    Bot.Config[file.toString().replace(".guild.json","")] = require("./config/"+file)
}
for (const file of commandFiles) {
	const command = require(`./bot/commands/${file}`)
    Bot.Commands.set(command.name, command)
}

// Client events to pass on to event files

Bot.Client.on("ready", () => {Bot.Events.Ready(Bot)})
Bot.Client.on("message", (msg) => {Bot.Events.Message(Bot,msg)})
Bot.Client.on("messageDelete", (msg) => {Bot.Events.MessageDelete(Bot,msg)})
Bot.Client.on("messageUpdate", (oldMsg,newMsg) => {Bot.Events.MessageUpdate(Bot,oldMsg,newMsg)})
Bot.Client.on("messageReactionAdd", (react,user) => {Bot.Events.messageReactionAdd(Bot,react.message,react,user)})
Bot.Client.on("guildMemberUpdate", (oldMember,newMember) => {Bot.Events.guildMemberUpdate(Bot,oldMember,newMember)})

// Logging events to pass on to event files

Bot.Client.on("error", (err) => {Bot.Helper.Logger.error("discord.js Error: "+err)})
Bot.Client.on("warn", (warn) => {Bot.Helper.Logger.warn("discord.js Warning: "+err)})
Bot.Client.on("reconnect", () => {Bot.Helper.Logger.log("Reconnecting to Discord WebSocket")})
Bot.Client.on("disconnect", () => {Bot.Helper.Logger.log("Disconnected from Discord WebSocket")})
Bot.Client.on("resume", () => {Bot.Helper.Logger.log("Resumed Discord WebSocket")})
Bot.Client.on("debug", (debug) => {Bot.Helper.Logger._log("discord.js Debug: "+debug)})
process.on("unhandledRejection", error => {Bot.Helper.Logger._warn("Unhandled promise rejection occured: \""+error+"\"")});

// Login
if (Bot.Config.Bot.token) {
    Bot.Client.login(Bot.Config.Bot.token)
        .catch(function(err){
            Bot.Helper.Logger.error("Failed to authenticate token")
            process.exit()
        })
} else if (process.argv[2]) {
    Bot.Client.login(process.argv[2])
        .catch(function(err){
            Bot.Helper.Logger.error("Failed to authenticate token")
            process.exit()
        })
} else {
    Bot.Helper.Logger.error("Please authenticate using a token")
    process.exit()
} 
