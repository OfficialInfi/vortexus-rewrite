
module.exports = async function (Bot){
    try {
        Bot.Helper.Request("https://officialinfi.gitlab.io/repo/VortexusRewrite.json", function(err, res, body) {
            if (err) throw err
            if (!res.statusCode == 200) throw "Status code is "+res.statusCode
            var json = JSON.parse(body);
            if (json.version != "1.3.0.0"){
                Bot.Helper.Logger.warn("The current version of Vortexus is outdated! The latest version is v"+json.version+"!")
            }
        })
    } catch (err){
        Bot.Helper.Logger.warn("Failed to check for updates. Error: \""+err+"\"")
    }
    Bot.Helper.Logger.log("Successfully logged into "+Bot.Client.user.tag+" serving "+Bot.Client.users.size+" users in "+Bot.Client.guilds.size+" guilds.")
    Bot.Client.user.setPresence({
        game: {name: Bot.Config.Bot.game}, status: "online"
    })
        .catch(function(err){Bot.Helper.Logger.error("Couldn't set status. "+error)})
    var application = await Bot.Client.fetchApplication()
    if (application.owner instanceof Bot.Discord.User){
        Bot.Config.Bot.trusted.push(application.owner.id)
    }
}