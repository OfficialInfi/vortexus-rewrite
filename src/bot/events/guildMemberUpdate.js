module.exports = function (Bot,oldMember,newMember){
    if (!Bot.Config[newMember.guild.id]) return
    if (oldMember.nickname == newMember.nickname || oldMember.user.name == newMember.nickname) return
    

    if (Bot.Config[newMember.guild.id].modules.moderation.automod.nicknames.enabled){
        if (msg.author.id == Bot.Client.user.id) return
        if (Bot.Helper.Automod.bypass(Bot,msg.guild,msg.member)) return
        var content = newMember.nickname.toLowerCase()
        for (word of Bot.Config[newMember.guild.id].modules.moderation.automod.nicknames.words){
            if (content.includes(word.toLowerCase())){
                newMember.setNickname(oldMember.user.username)
                newMember.send(Bot.Helper.Embed("error","Your nickname violated this server's word filter!",newMember.guild.name,newMember.guild.iconURL()))
            }
        }
    }
}
