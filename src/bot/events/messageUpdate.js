module.exports = function (Bot,omsg,msg){
    if (msg.partial) return
    if (!msg.guild) return
    if (!Bot.Config[msg.guild.id]) return
    if (msg.author.bot) return
    if (!Bot.Config[msg.guild.id].modules.moderation.log.messageUpdate.enabled) return
    if (msg.author.id == Bot.Client.user.id) return
    if (Bot.Helper.Automod.bypass(Bot,msg.guild,msg.member,msg.channel,msg)) return
    if (!Bot.Config[msg.guild.id].modules.moderation.log.messageUpdate.channel) return
    if (!msg.guild.channels.get((Bot.Config[msg.guild.id].modules.moderation.log.messageUpdate.channel))) return
    if (!Bot.Config[msg.guild.id]) return
        if (Bot.Config[msg.guild.id].modules.moderation.automod.messages.enabled){
            var content = msg.content.toLowerCase()
            for (word of Bot.Config[msg.guild.id].modules.moderation.automod.messages.words){
                if (content.includes(word.toLowerCase())) msg.delete()
            }
    }
    try {
        msg.guild.channels.get(Bot.Config[msg.guild.id].modules.moderation.log.messageUpdate.channel).send(Bot.Helper.Embed("info","Message Updated in #"+msg.channel.name,"Updated by "+msg.author.tag+" // "+msg.author.id,msg.author.avatarURL(),omsg.content+" **>>** "+msg.content,null,msg.url))
    } catch(err){
        Bot.Helper.Logger._error("Failed to send message in channel \""+Bot.Config[msg.guild.id].modules.moderation.log.messageUpdate.channel+"\" Error: "+err)
    }
}