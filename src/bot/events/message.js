var discord = require("discord.js")
AutomodSpam = new discord.Collection()
module.exports = async function (Bot,msg){

    if (msg.guild){
        if (msg.author.id == Bot.Client.user.id) return
        if (!Bot.Helper.Automod.bypass(Bot,msg.guild,msg.member,msg.channel,msg)){
            if (!Bot.Config[msg.guild.id]) return

            // Automod - Masspinging

            if (Bot.Config[msg.guild.id].modules.moderation.automod.messages.enabled){
                var length = Number(msg.mentions.users.array().length)+Number(msg.mentions.roles.array().length)
                if (length > Bot.Config[msg.guild.id].modules.moderation.automod.messages.maxmentions) msg.delete()
            }

            // Automod - Filter
            
            if (Bot.Config[msg.guild.id].modules.moderation.automod.messages.enabled){
                var content = msg.content.toLowerCase()
                for (word of Bot.Config[msg.guild.id].modules.moderation.automod.messages.words){
                    if (content.includes(word.toLowerCase())) msg.delete()
                }
            }
            
            // Automod - Spam

            if (Bot.Config[msg.guild.id].modules.moderation.automod.messages.enabled){
                if (!AutomodSpam.has(msg.author.id)) {
                    AutomodSpam.set(msg.author.id, new Bot.Discord.Collection())
                }
                const now = Date.now()
                const timestamps = AutomodSpam.get(msg.author.id)
                const cooldownAmount = (Bot.Config[msg.guild.id].modules.moderation.automod.messages.spam) * 1000
                if (timestamps.has(msg.author.id)) {
                    const expirationTime = timestamps.get(msg.author.id) + cooldownAmount

                    if (now < expirationTime) {
                        const timeLeft = (expirationTime - now) / 1000
                        msg.delete()
                        return msg.author.send(Bot.Helper.Embed("error","Please wait "+timeLeft.toFixed(1)+" more seconds before using sending a message!","#"+msg.channel.name+" - "+msg.guild.name,msg.guild.iconURL()))
                    }
                }    
                timestamps.set(msg.author.id, now)
                setTimeout(() => timestamps.delete(msg.author.id), cooldownAmount)
            }  
        } 
    }

    // Leveling

    if (msg.guild){
        if (!Bot.Config[msg.guild.id]) return
        if (Bot.Config[msg.guild.id].modules.fun.levels.enabled){
            Bot.Helper.Database.add(msg.guild.id,"exp_"+msg.author.id,1)
            var exp = await Bot.Helper.Database.get(msg.guild.id,"exp_"+msg.author.id,1)
            var lvl = await Bot.Helper.Database.get(msg.guild.id,"lvl_"+msg.author.id)
            if (exp){
                if (Number(exp) > Number(Bot.Config[msg.guild.id].modules.fun.levels.requiredEXP)*Number(lvl)){
                    Bot.Helper.Database.fset(msg.guild.id,"exp_"+msg.author.id,0)
                    Bot.Helper.Database.fset(msg.guild.id,"lvl_"+msg.author.id,lvl+1)
                    try {
                        msg.author.send(Bot.Helper.Embed("info","You just leveled up to Level "+lvl+1+"!",msg.guild.name,msg.guild.iconURL()))
                    } catch (err) {
                        Bot.Helper.Logger._warn("Couldn't send DM to "+msg.author.tag)
                    }
                }
            }
        }
    }

    // Command check

    if (!msg.content.startsWith(Bot.Config.Bot.prefix) || msg.author.bot) return

    // Check if command exists

    const args = msg.content.slice(Bot.Config.Bot.prefix.length).split(/ +/)
    const commandName = args.shift().toLowerCase()
    const command = Bot.Commands.get(commandName) || Bot.Commands.find(command => command.aliases && command.aliases.includes(commandName))
    if (!command) return
    if (!command.module) return

    // Guildonly check

    if (command.guildOnly && msg.channel.type !== "text") {
        return msg.channel.send(Bot.Helper.Embed("error","`"+commandName+"` can only be used inside of servers.","Requested by "+msg.author.tag, msg.author.avatarURL()))
    } else if (command.guildOnly && msg.channel.type == "text" &! Bot.Config[msg.guild.id]) {
        return msg.channel.send(Bot.Helper.Embed("error","There was an issue with running `"+commandName+"`","Requested by "+msg.author.tag, msg.author.avatarURL(),"This command can only be used inside of guilds, and this guild's configuration isn't set up properly!"))
    }

    // Cooldown check

    if (!Bot.Cooldowns.has(command.name)) {
        Bot.Cooldowns.set(command.name, new Bot.Discord.Collection())
    }
    const now = Date.now()
    const timestamps = Bot.Cooldowns.get(command.name)
    const cooldownAmount = (command.cooldown || Bot.Config.Bot.command_cooldown) * 1000
    if (timestamps.has(msg.author.id)) {
        const expirationTime = timestamps.get(msg.author.id) + cooldownAmount

        if (now < expirationTime) {
            const timeLeft = (expirationTime - now) / 1000
            return msg.channel.send(Bot.Helper.Embed("error","Please wait "+timeLeft.toFixed(1)+" more seconds before using `"+commandName+"`!","Requested by "+msg.author.tag, msg.author.avatarURL()))
        }
    }    
    timestamps.set(msg.author.id, now)
    setTimeout(() => timestamps.delete(msg.author.id), cooldownAmount)

    // Enabled Check

    if (!command.module == "debug"){
        if (command.guildOnly && command.module && Bot.Config[msg.guild.id] && msg.channel.type == "text") {
            if (!Bot.Config[msg.guild.id].modules[command.module][command.name] && !Bot.Config[msg.guild.id].modules[command.module][command.name].enabled) return
        } else if (!command.guildOnly){
            if (!Bot.Config.Bot.modules[command.module][command.name] && !Bot.Config.Bot.modules[command.module][command.name].enabled) return
        }
    }

    // Permission check

    if (command.permissions && Bot.Config[msg.guild.id] && msg.channel.type == "text" && command.guildOnly == true){
        if (!msg.member.hasPermission(command.permissions)){
            return msg.channel.send(Bot.Helper.Embed("error","You do not have permission to run `"+commandName+"`!","Requested by "+msg.author.tag, msg.author.avatarURL()))
        }
        if (!msg.guild.me.hasPermission(command.permissions)){
            return msg.channel.send(Bot.Helper.Embed("error","I do not have permission to run `"+commandName+"`!","Requested by "+msg.author.tag, msg.author.avatarURL()))
        }
    } else if (!command.guildOnly && command.module != "debug"){
        if (!command.permissions)
        if (!command.permission == "trusted") return
        var result
        Bot.Config.Bot.trusted.forEach(function(value){
            if (value == msg.author.id){
                result = true
            }
        })
        if (!result == true){
            return msg.channel.send(Bot.Helper.Embed("error","You do not have permission to run `"+commandName+"`!","Requested by "+msg.author.tag, msg.author.avatarURL()))
        }
    } else if (!command.guildOnly && command.module == "debug"){
        var result
        Bot.Config.Bot.trusted.forEach(function(value){
            if (value == msg.author.id){
                result = true
            }
        })
        if (!result == true){
            return msg.channel.send(Bot.Helper.Embed("error","You do not have permission to run `"+commandName+"`!","Requested by "+msg.author.tag, msg.author.avatarURL()))
        } else if (result == true){
            if (!Bot.Config.Bot.debug == true){
                return msg.channel.send(Bot.Helper.Embed("error","Debug mode is disabled!","Requested by "+msg.author.tag, msg.author.avatarURL()))
            }
        }
    }

    // Argument check

    if (command.args && !args.length) {
        return msg.channel.send(Bot.Helper.Embed("error","`"+commandName+"` requires arguments!","Requested by "+msg.author.tag, msg.author.avatarURL(),"The proper usage is `"+Bot.Config.Bot.prefix+commandName+" "+command.usage+"`"))
    }
    try {
        if (command.name != "poll"){
            msg.react("👀")
        }
        Bot.Commands.get(command.name).execute(Bot,msg,args)
    } catch (error) {
        msg.channel.stopTyping(true)
        msg.channel.send(Bot.Helper.Embed("error","There was an issue with running `"+commandName+"`","Requested by "+msg.author.tag, msg.author.avatarURL(),"For geeks: `"+error+"`"))
    }
}