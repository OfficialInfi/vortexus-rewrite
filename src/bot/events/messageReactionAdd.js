
module.exports = async function (Bot,msg,react,user){
    if (msg.channel.type != "text") return
    if (msg.partial){
        msg = await msg.channel.messages.fetch(msg.id)
    }
    if (!Bot.Config[msg.guild.id]) return
    if (!Bot.Config[msg.guild.id].modules.fun.starboard.enabled) return
    if (react.emoji.name !== "⭐") return
    if (Bot.Config[msg.guild.id].modules.fun.starboard.selfstar == false){
        const users = await react.users.fetch()
        if (users.get(msg.author.id)){
            try {
                react.users.remove(msg.author)
            } catch (err) {
                Bot.Helper.Logger._warn("Failed to remove reaction from message \""+msg.id+"\" Error: \""+err+"\"")
            }
            return
        }
    }
    if (!Bot.Config[msg.guild.id].modules.fun.starboard.channel) return
    if (!msg.guild.channels.get(Bot.Config[msg.guild.id].modules.fun.starboard.channel)) return
    if (react.count > Bot.Config[msg.guild.id].modules.fun.starboard.minimum - 1){
        try {
        	return msg.guild.channels.get(Bot.Config[msg.guild.id].modules.fun.starboard.channel).send(Bot.Helper.Embed("star","⭐️"+" #"+msg.channel.name,"Posted by "+msg.author.tag, msg.author.avatarURL()(),msg.content,null,msg.url))
        } catch (err) {
            Bot.Helper.Logger._warn("Failed to add message \""+msg.id+" to guild \""+msg.guild.id+"\"'s starboard. Error: "+err)
        }
    }
}