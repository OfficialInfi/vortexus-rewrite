module.exports = async function (Bot,msg){
    if (!Bot.Config[msg.guild.id]) return
    if (!Bot.Config[msg.guild.id].modules.moderation.log.messageDelete.enabled) return
    if (!Bot.Config[msg.guild.id].modules.moderation.log.messageDelete.channel) return
    if (!msg.guild.channels.get(Bot.Config[msg.guild.id].modules.moderation.log.messageDelete.channel)) return
    if (msg.partial) return
    try {
        msg.guild.channels.get(Bot.Config[msg.guild.id].modules.moderation.log.messageDelete.channel).send(Bot.Helper.Embed("info","Message Deleted in #"+msg.channel.name,"Authored by "+msg.author.tag+" // "+msg.author.id,msg.author.avatarURL(),msg.content))
    } catch(err){
        Bot.Helper.Logger._error("Failed to send message in channel \""+Bot.Config[msg.guild.id].modules.moderation.log.messageDelete.channel+"\" Error: "+err)
    }
}