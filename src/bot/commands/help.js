module.exports = {
    name: "help",
    module: "core",
    aliases: ["help","cmds","commands","?"],
    args: false,
    usage: "[command]",
    description: "Get help with the bot or a specific command.",
	execute(Bot,msg,args) {
		if (!args.length || args[0] == "help") {
			return msg.author.send(Bot.Helper.Embed("success","Vortexus Help","Requested by "+msg.author.tag,msg.author.avatarURL(),"I am a multipurpose bot created by Infinixius!\n💬[Commands](https://gitlab.com/Infinixius/vortexus-rewrite/wiki/Commands)\n🖥[Source Code](https://gitlab.com/Infinixius/vortexus-rewrite/)\nYou can also use `"+Bot.Config.Bot.prefix+"help [command]` to get help about a specific command!"))
				.catch(error => {
					return msg.channel.send(Bot.Helper.Embed("error","I can't DM you!","Requested by "+msg.author.tag, msg.author.avatarURL()))
				})
		} else {
		const name = args[0].toLowerCase();
		const command = Bot.Commands.get(name) || Bot.Commands.find(c => c.aliases && c.aliases.includes(name));

		if (!command) {
			return msg.channel.send(Bot.Helper.Embed("error","`"+name+"` is not a command!","Requested by "+msg.author.tag, msg.author.avatarURL()))
		}


		msg.channel.send(Bot.Helper.Embed("success","**"+name+" Help**","Requested by "+msg.author.tag, msg.author.avatarURL(),"**Description**: "+command.description+"\n**Usage**: `"+Bot.Config.Bot.prefix+name+" "+command.usage+"`\n**Aliases**: "+command.aliases.join(", ")))
	}
},
};