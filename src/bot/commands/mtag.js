module.exports = {
	name: "mtag",
	module: "utility",
	aliases: ["mtag","mnote","msnippet"],
	args: true,
	usage: "{set/del} {name} [text]",
	description: "Set and delete tags/notes (under 150 characters) that can be read later.",
	permissions: ["MANAGE_MESSAGES"],
	cooldown: 5,
	guildOnly: true,
	async execute(Bot,msg,args) {
        if (!args[0] && !args[1]) return
        if (args[0] == "set"){
            var text = args.slice(2).toString().replace(/,/g, " ")
            if (text.length >= 150){
			    return msg.channel.send(Bot.Helper.Embed("error","The text for tag **"+args[1]+"** is over 150 characters. ("+text.length+")","Requested by "+msg.author.tag, msg.author.avatarURL()))
            } else {
                await Bot.Helper.Database.fset(msg.guild.id,"tag_"+args[1],text)
			    msg.channel.send(Bot.Helper.Embed("info","Tag "+args[1],"Requested by "+msg.author.tag, msg.author.avatarURL(),text))
            }
        } else if (args[0] == "del"){
            if (!args[0]) return
            if (await Bot.Helper.Database.get(msg.guild.id),"tag_"+args[1]){
                await Bot.Helper.Database.del(msg.guild.id,"tag_"+args[1])
			    msg.channel.send(Bot.Helper.Embed("success","Tag **"+args[1]+"** was deleted.","Requested by "+msg.author.tag, msg.author.avatarURL()))
            } else {
			    msg.channel.send(Bot.Helper.Embed("error","The tag **"+args[1]+"** does not exist.","Requested by "+msg.author.tag, msg.author.avatarURL()))
            }
        } else {
			    msg.channel.send(Bot.Helper.Embed("error","**"+args[0]+"** is not a command.","Requested by "+msg.author.tag, msg.author.avatarURL(),"Try \"set\" or \"del\""))
        }
	},
}