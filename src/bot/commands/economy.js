module.exports = {
	name: "economy",
	module: "economy",
	aliases: ["economy","eco","meconomy","meco"],
	args: true,
	usage: "{set/add/del} {user mention/id} {value}",
	description: "Manage user's balances.",
	permissions: ["MANAGE_SERVER"],
	guildOnly: true,
	async execute(Bot,msg,args) {
	  if (!args[0]) return
	  if (!args[2]) return
	  var user = null
		if (msg.mentions.members.size) {
			if (msg.mentions.members.first()) {
				user = msg.mentions.members.first()
			}
		} else if (msg.guild.members.get(args[1])) {
			user = msg.guild.members.get(args[1])
		} else {
			return msg.channel.send(Bot.Helper.Embed("error","You need a user!","Requested by "+msg.author.tag, msg.author.avatarURL()))
		}
	  if (args[0] == "set"){
		try {
			await Bot.Helper.Database.fset(msg.guild.id,"economy_"+user.user.id,Number(args[2]))
			msg.channel.send(Bot.Helper.Embed("success","Set "+user.user.tag+"'s balance to "+args[2],"Requested by "+msg.author.tag,msg.author.avatarURL()))
		} catch(err) {
			msg.channel.send(Bot.Helper.Embed("error","Couldn't set "+user.user.tag+"'s balance","Requested by "+msg.author.tag,msg.author.avatarURL(),"Error: "+err))
		}
	  } else if (args[0] == "add"){
		try {
			await Bot.Helper.Database.fset(msg.guild.id,"economy_"+user.user.id,Number(args[2])+Number(await Bot.Helper.Database.get(msg.guild.id,"economy_"+user.user.id)))
			msg.channel.send(Bot.Helper.Embed("success","Added "+args[2]+" to "+user.user.tag+"'s balance","Requested by "+msg.author.tag,msg.author.avatarURL()))
		} catch(err) {
			msg.channel.send(Bot.Helper.Embed("error","Couldn't change "+user.user.tag+"'s balance","Requested by "+msg.author.tag,msg.author.avatarURL(),"Error: "+err))
		}
	  } else if (args[0] == "del"){
		try {
			await Bot.Helper.Database.fset(msg.guild.id,"economy_"+user.user.id,Number(await Bot.Helper.Database.get(msg.guild.id,"economy_"+user.user.id)-Number(args[2])))
			msg.channel.send(Bot.Helper.Embed("success","Removed "+args[2]+" to "+user.user.tag+"'s balance","Requested by "+msg.author.tag,msg.author.avatarURL()))
		} catch(err) {
			msg.channel.send(Bot.Helper.Embed("error","Couldn't change "+user.user.tag+"'s balance","Requested by "+msg.author.tag,msg.author.avatarURL(),"Error: "+err))
		}
	  }
	},
}