module.exports = {
	name: "reddit",
	module: "fun",
	aliases: ["reddit","subreddit"],
	args: true,
	usage: "{subreddit}",
	description: "Get a random post from a subreddit.",
	guildOnly: true,
	async execute(Bot,msg,args) {
		if (!args[0]) return
		try {
			await Bot.Helper.Request("https://www.reddit.com/r/"+args[0]+"/hot.json?limit=100", function(err, res, body) {
				if (err) return err
				if (res.statusCode != 200){
					return msg.channel.send(Bot.Helper.Embed("error","Failed to get post from Reddit.","Requested by "+msg.author.tag, msg.author.avatarURL(),"Status code is "+res.statusCode))
				}
				var json = JSON.parse(body)
				var posts = json.data.children
				var chose = undefined
				for (i = 0; i < posts.length; i++) {
					var rand = Math.floor(Math.random() * 100)
					var post = posts[rand].data
					if (post.stickied) continue
					if (post.pinned) continue
					chose = true
					if (post.url.includes(".jpg") || post.url.includes(".png")){
						msg.channel.send(Bot.Helper.Embed("info",post.title,"Requested by "+msg.author.tag, msg.author.avatarURL(),post.selftext,post.thumbnail,"https://reddit.com"+post.permalink,post.url))
					} else {
						msg.channel.send(Bot.Helper.Embed("info",post.title,"Requested by "+msg.author.tag, msg.author.avatarURL(),post.selftext,post.thumbnail,"https://reddit.com"+post.permalink))
					}
					break
				}
				if (chose == undefined){
					msg.channel.send(Bot.Helper.Embed("error","Failed to get post from Reddit.","Requested by "+msg.author.tag, msg.author.avatarURL()))
				}
			})
		} catch (err){
			msg.channel.send(Bot.Helper.Embed("error","Failed to get post from Reddit.","Requested by "+msg.author.tag, msg.author.avatarURL(),err))
		}
	},
}