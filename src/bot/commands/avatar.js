module.exports = {
	name: "avatar",
	module: "utility",
	aliases: ["avatar","pfp","profilepicture","profilepic","avi"],
	args: false,
	usage: "user mention/id",
	description: "Get a user's profile picture",
	guildOnly: true,
	async execute(Bot,msg,args) {
		var user
		if (msg.mentions.members.size) {
            if (msg.mentions.members.first()) {
                user = msg.mentions.members.first()
            }
		} else if (msg.guild.members.get(args[0])) {
			user = msg.guild.members.get(args[0])
		} else {
        	user = msg.author
		}
		if (user){
			msg.channel.send(Bot.Helper.Embed("info","Avatar of **"+user.user.tag+"**","Requested by "+msg.author.tag, msg.author.avatarURL(),null,null,null,user.user.avatarURL({size: 256})))
		}
	},
}