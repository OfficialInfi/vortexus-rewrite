module.exports = {
	name: "gamble",
	module: "economy",
	aliases: ["gamble"],
	args: true,
	usage: "{number}",
	description: "Pick a random number between 1-10, if you are right you win $10. If you are wrong, you lose $5.",
	guildOnly: true,
	cooldown: 1,
	async execute(Bot,msg,args) {
		var balance = await Bot.Helper.Database.get(msg.guild.id,"economy_"+msg.author.id)
		var rand = Math.floor(Math.random() * 10)
		if (Number(args[0]) == rand){
			if (balance){
				Bot.Helper.Database.fset(msg.guild.id,"economy_"+msg.author.id,balance+10)
			} else {
				Bot.Helper.Database.fset(msg.guild.id,"economy_"+msg.author.id,10)
			}
			msg.channel.send(Bot.Helper.Embed("success","You won $10!","Requested by "+msg.author.tag,msg.author.avatarURL()))
		} else {
			if (balance){
				Bot.Helper.Database.fset(msg.guild.id,"economy_"+msg.author.id,balance-5)
			}
			msg.channel.send(Bot.Helper.Embed("info","You lost $5!","Requested by "+msg.author.tag,msg.author.avatarURL(),"The number was "+rand+"."))
		}
	},
}