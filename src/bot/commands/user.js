module.exports = {
	name: "user",
	module: "utility",
	aliases: ["user","member","profile"],
	args: true,
	usage: "{user mention/id}",
	description: "Get information on a user",
	cooldown: 5,
	guildOnly: true,
	async execute(Bot,msg,args) {
		var user
		if (msg.mentions.members.size) {
            if (msg.mentions.members.first()) {
                user = msg.mentions.members.first()
            }
		} else if (msg.guild.members.get(args[0])) {
			user = msg.guild.members.get(args[0])
		} else {
        	return msg.channel.send(Bot.Helper.Embed("error","You need a user!","Requested by "+msg.author.tag, msg.author.avatarURL()))
		}
		if (user){
			msg.channel.send(Bot.Helper.Embed("info",user.user.tag,"Requested by "+msg.author.tag, msg.author.avatarURL(),"**Created At**: "+user.user.createdAt+"\n**ID**: "+user.user.id,user.user.avatarURL()))
		}
	},
}