module.exports = {
	name: "topic",
	module: "fun",
	aliases: ["topic","deadchat"],
	args: false,
	usage: "none",
	description: "Get a random question from r/askreddit",
	guildOnly: true,
	async execute(Bot,msg,args) {
		try {
			await Bot.Helper.Request("https://www.reddit.com/r/askreddit/hot.json?limit=100", function(err, res, body) {
				if (err) return err
				if (res.statusCode != 200){
					return msg.channel.send(Bot.Helper.Embed("error","Failed to get post from Reddit.","Requested by "+msg.author.tag, msg.author.avatarURL(),"Status code is "+res.statusCode))
				}
				var json = JSON.parse(body)
				var posts = json.data.children
				var chose = undefined
				for (i = 0; i < posts.length; i++) {
					var rand = Math.floor(Math.random() * 100)
					var post = posts[rand].data
					if (post.stickied) continue
					if (post.pinned) continue
					if (post.ups < 10) continue
					chose = true
					msg.channel.send(Bot.Helper.Embed("info",post.title,"Requested by "+msg.author.tag, msg.author.avatarURL()))
					break
				}
				if (chose == undefined){
					msg.channel.send(Bot.Helper.Embed("error","Failed to get post from Reddit.","Requested by "+msg.author.tag, msg.author.avatarURL()))
				}
			})
		} catch (err){
			msg.channel.send(Bot.Helper.Embed("error","Failed to get post from Reddit.","Requested by "+msg.author.tag, msg.author.avatarURL(),err))
		}
	},
}