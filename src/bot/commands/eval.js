module.exports = {
	name: "eval",
	module: "core",
	aliases: ["eval","code","js","javascript","execute","exec"],
	args: true,
	usage: "{code}",
	description: "Evaluate Javascript code.",
	permissions: "trusted",
	async execute(Bot,msg,args) {
		var code = args.slice(0).toString().replace(/,/g, " ")
		try {
			if (!code) return
			var result = Bot.Helper.VM.runInNewContext(code,{msg})
			if (!result){result = "No result."}
			msg.channel.send(Bot.Helper.Embed("success",result,"Requested by "+msg.author.tag,msg.author.avatarURL(),code))
		} catch(err) {
			msg.channel.send(Bot.Helper.Embed("error",err,"Requested by "+msg.author.tag,msg.author.avatarURL()))
		}
	},
}