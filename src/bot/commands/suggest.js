module.exports = {
	name: "suggest",
	module: "utility",
	aliases: ["suggest"],
	args: true,
	usage: "{suggestion}",
	description: "Add a suggestion to the suggestions channel.",
	guildOnly: true,
	async execute(Bot,msg,args) {
		if (!Bot.Config[msg.guild.id]) return
		if (!Bot.Config[msg.guild.id].modules.utility.suggest) return
		if (!msg.guild.channels.get(Bot.Config[msg.guild.id].modules.utility.suggest)) return
		var suggestion = args.slice(0).toString().replace(/,/g, " ")
		try {
			var channel = msg.guild.channels.get(Bot.Config[msg.guild.id].modules.utility.suggest)
			channel.send(Bot.Helper.Embed("info",suggestion,"Suggested by "+msg.author.tag+" // "+msg.author.id, msg.author.avatarURL()))
		} catch(err){
			msg.reply(Bot.Helper.Embed("error","Failed to send suggestion","Requested by "+msg.author.tag, msg.author.avatarURL()))
		}
	},

}