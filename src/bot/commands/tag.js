module.exports = {
	name: "tag",
	module: "utility",
	aliases: ["tag","note","snippet"],
	args: true,
	usage: "{name}",
	description: "Read tags/notes that were set with mtag.",
	cooldown: 5,
	guildOnly: true,
	async execute(Bot,msg,args) {
        if (!args[0]) return
        if (await Bot.Helper.Database.get(msg.guild.id,"tag_"+args[0])){
            await msg.channel.send(Bot.Helper.Embed("info","Tag **"+args[0]+"**","Requested by "+msg.author.tag, msg.author.avatarURL(),await Bot.Helper.Database.get(msg.guild.id,"tag_"+args[0])))
        } else {
            await msg.channel.send(Bot.Helper.Embed("error","The tag **"+args[0]+"** does not exist.","Requested by "+msg.author.tag, msg.author.avatarURL()))
        }
	},
}