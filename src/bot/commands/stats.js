module.exports = {
	name: "stats",
	module: "core",
	aliases: ["stats","bot","statistics"],
	description: "Show statistics related to the bot",
	guildOnly: true,
	async execute(Bot,msg,args) {
		var uptime = process.uptime();
		msg.channel.send(Bot.Helper.Embed("error","Vortexus Statistics","Requested by "+msg.author.tag, msg.author.avatarURL(),"**Uptime**: "+Bot.Helper.Time.formatSeconds(uptime)+"\n**Version**: "+Bot.Version+"\n**Discord.JS Version**: "+Bot.Discord.Constants.Package.version))
	},
}