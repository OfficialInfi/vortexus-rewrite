module.exports = {
	name: "xkcd",
	module: "fun",
	aliases: ["xkcd","xkcdcomic"],
	args: false,
	usage: "[number from 1-2220]",
	description: "Get a random xkcd comic from 1-2220.",
	guildOnly: true,
	async execute(Bot,msg,args) {
		var comic
		if (!args[0]){
			comic = Math.floor(Math.random()*2220)
		} else if (args[0]){
			comic = args[0]
		}
		try {
			await Bot.Helper.Request("https://xkcd.com/"+comic+"/info.0.json", function(err, res, body) {
				if (err) return err
				if (res.statusCode != 200){
					return msg.channel.send(Bot.Helper.Embed("error","Failed to get post from xkcd.","Requested by "+msg.author.tag, msg.author.avatarURL(),"Status code is "+res.statusCode))
				}
				var json = JSON.parse(body)
				msg.channel.send(Bot.Helper.Embed("info","#"+json.num+" - "+json.safe_title,"Requested by "+msg.author.tag, msg.author.avatarURL(),json.alt,null,"https://xkcd.com/"+json.num,json.img))
			})
		} catch (err){
			msg.channel.send(Bot.Helper.Embed("error","Failed to get post from xkcd.","Requested by "+msg.author.tag, msg.author.avatarURL(),err))
		}
	},
}