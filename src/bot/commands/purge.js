module.exports = {
	name: "purge",
	aliases: ["purge"],
	args: true,
	usage: "{channel mention/id} {number}",
	guildOnly: true,
	description: "Remove up to 100 messages that are newer than 2 weeks.",
	module: "moderation",
	permissions: ["MANAGE_MESSAGES"],
	async execute(Bot,msg,args) {
		var channel = null
		if (msg.mentions.channels.size) {
            if (msg.mentions.channels.first()) {
                channel = msg.mentions.channels.first()
            }
		} else if (msg.guild.channels.get(args[0])) {
			channel = msg.guild.channels.get(args[0])
		} else {
        	return msg.channel.send(Bot.Helper.Embed("error","You need a channel to purge!","Requested by "+msg.author.tag, msg.author.avatarURL()))
		}
		if (!args[1]) return
		if (Number(args[1]) > 100){
			return msg.channel.send(Bot.Helper.Embed("error","You can only purge 100 messages at once.","Requested by "+msg.author.tag, msg.author.avatarURL()))
		}
		try {
			channel.bulkDelete(Number(args[1]))
			return msg.channel.send(Bot.Helper.Embed("success","Successfully purged "+channel.name,"Requested by "+msg.author.tag, msg.author.avatarURL()))
		} catch (err) {
			return msg.channel.send(Bot.Helper.Embed("error","Failed to purge #"+channel.name,"Requested by "+msg.author.tag, msg.author.avatarURL(),err))
		}
	},
};