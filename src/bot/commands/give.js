module.exports = {
	name: "give",
	module: "economy",
	aliases: ["give","transfer"],
	args: true,
	usage: "{user mention/id} {money}",
	description: "Give another user some money.",
	guildOnly: true,
	async execute(Bot,msg,args) {
		var user
		if (msg.mentions.members.size) {
            if (msg.mentions.members.first()) {
				user = msg.mentions.members.first()
            }
		} else if (msg.guild.members.get(args[0])) {
			user = msg.guild.members.get(args[0])
		} else {
			msg.channel.send(Bot.Helper.Embed("error","You need a user to give money to!","Requested by "+msg.author.tag, msg.author.avatarURL()))
		}
		if (!args[1]) return
		var num = Math.round(Number(args[1]))
		var balance = await Bot.Helper.Database.get(msg.guild.id,"economy_"+msg.author.id)
		var userBalance = await Bot.Helper.Database.get(msg.guild.id,"economy_"+user.id)
		if (balance){
			if (balance > num || balance == num){
				await Bot.Helper.Database.fset(msg.guild.id,"economy_"+msg.author.id,Number(balance)-num)
				await Bot.Helper.Database.fset(msg.guild.id,"economy_"+user.id,userBalance+num)
				msg.channel.send(Bot.Helper.Embed("success","Successfully gave "+num+" to "+user.user.tag+".","Requested by "+msg.author.tag, msg.author.avatarURL()))
			} else {
				return msg.channel.send(Bot.Helper.Embed("error","You do not have enough money!","Requested by "+msg.author.tag, msg.author.avatarURL()))
			}
		}
		
	},
}