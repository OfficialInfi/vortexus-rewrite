module.exports = {
	name: "automod",
	module: "debug",
	aliases: ["automod"],
	description: "View automod status.",
	guildOnly: true,
	execute(Bot,msg,args) {
		msg.channel.send("**Automod Status**: "+Bot.Helper.Automod.bypass(Bot,msg.guild,msg.member,msg.channel,msg))
	},
}