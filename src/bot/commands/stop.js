module.exports = {
	name: "stop",
	module: "core",
	permission: "trusted",
	aliases: ["stop","end","exit","shutdown"],
	description: "Shutdown the bot. You need to be trusted in the config.",
	async execute(Bot,msg,args) {
		await msg.channel.send(Bot.Helper.Embed("success","The bot has successfully shut down.","Requested by "+msg.author.tag, msg.author.avatarURL()))
		msg.client.user.setPresence({status: "invisible"})
		msg.client.destroy()
		process.exit()
	},
}