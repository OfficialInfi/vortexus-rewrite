module.exports = {
	name: "meme",
	module: "fun",
	aliases: ["meme","dankmeme"],
	args: false,
	usage: "none",
	description: "Get a meme with 1000+ upvotes on r/dankmemes",
	guildOnly: true,
	async execute(Bot,msg,args) {
		try {
			await Bot.Helper.Request("https://www.reddit.com/r/dankmemes/hot.json?limit=100", function(err, res, body) {
				if (err) return err
				if (res.statusCode != 200){
					return msg.channel.send(Bot.Helper.Embed("error","Failed to get post from Reddit.","Requested by "+msg.author.tag, msg.author.avatarURL(),"Status code is "+res.statusCode))
				}
				var json = JSON.parse(body)
				var posts = json.data.children
				var chose = undefined
				for (i = 0; i < posts.length; i++) {
					var rand = Math.floor(Math.random() * 100)
					var post = posts[rand].data
					if (post.stickied) continue
					if (post.pinned) continue
					if (post.ups < 1000) continue
					if (!post.url) continue
					chose = true
					msg.channel.send(Bot.Helper.Embed("info",post.title,"Requested by "+msg.author.tag, msg.author.avatarURL(),null,null,"https://reddit.com"+post.permalink,post.url))
					break
				}
				if (chose == undefined){
					msg.channel.send(Bot.Helper.Embed("error","Failed to get post from Reddit.","Requested by "+msg.author.tag, msg.author.avatarURL()))
				}
			})
		} catch (err){
			msg.channel.send(Bot.Helper.Embed("error","Failed to get post from Reddit.","Requested by "+msg.author.tag, msg.author.avatarURL(),err))
		}
	},
}