module.exports = {
	name: "rng",
	module: "utility",
	aliases: ["rng","random","rndm"],
	description: "Generate a random number between max and min.",
	args: true,
	usage: "{max} [min]",
	guildOnly: true,
	async execute(Bot,msg,args) {
		var min
		if (!args[1]){
			min = 0
		} else {
			min = args[1]
		}
		var rand = Math.floor(Math.random() * (args[0] - min) + min)
		msg.channel.send(Bot.Helper.Embed("info","Random Number: "+rand,"Requested by "+msg.author.tag, msg.author.avatarURL(),"Randomly generated between "+min+" and "+args[0]))
	},
}