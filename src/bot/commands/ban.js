module.exports = {
	name: "ban",
	aliases: ["ban","b"],
	args: true,
	usage: "{user mention/id} [reason]",
	guildOnly: true,
	description: "Ban a user.",
	module: "moderation",
	permissions: ["BAN_MEMBERS"],
	execute(Bot,msg,args) {
		var user = null
		if (msg.mentions.members.size) {
            if (msg.mentions.members.first()) {
                user = msg.mentions.members.first()
            }
		} else if (msg.guild.members.get(args[0])) {
			user = msg.guild.members.get(args[0])
		} else {
        	return msg.channel.send(Bot.Helper.Embed("error","You need a user to ban!","Requested by "+msg.author.tag, msg.author.avatarURL()))
		}
		var reason = args.slice(1).toString().replace(/,/g, " ")
		if (user.bannable && user.id != msg.author.id && user.id != Bot.Client.user.id){
			if (reason == "" && !Bot.Config[msg.guild.id].modules.moderation.ban.reasonRequired){
				reason = "No reason provided."
			} else if (reason == "" && Bot.Config[msg.guild.id].modules.moderation.ban.reasonRequired){
				return msg.channel.send(Bot.Helper.Embed("error","You need to provide a reason!","Requested by "+msg.author.tag, msg.author.avatarURL()))
			}
			try {
				user.ban(msg.author.tag+" / "+msg.author.id+" : "+reason)
				msg.channel.send(Bot.Helper.Embed("success","Successfully banned "+user.user.tag,"Requested by "+msg.author.tag, msg.author.avatarURL()))
				return user.send(Bot.Helper.Embed("info","You were banned from **"+msg.guild.name+"**","Banned by "+msg.author.tag, msg.author.avatarURL()),"Reason: **"+reason+"**")
			} catch (err) {
        		return msg.channel.send(Bot.Helper.Embed("error","Failed to ban "+user.user.tag,"Requested by "+msg.author.tag, msg.author.avatarURL()))
			}
		} else {
        	return msg.channel.send(Bot.Helper.Embed("error","I cannot ban "+user.user.tag+"!","Requested by "+msg.author.tag, msg.author.avatarURL()))
		}
	},
};