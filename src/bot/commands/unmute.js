module.exports = {
	name: "unmute",
	aliases: ["unmute","um","unsilence"],
	args: true,
	usage: "{user mention/id} [reason]",
	guildOnly: true,
	description: "Unmute a user.",
	module: "moderation",
	permissions: ["MANAGE_MESSAGES"],
	execute(Bot,msg,args) {
		var role = null
		if (msg.guild.roles.find(r => r.id == Bot.Config[msg.guild.id].modules.moderation.unmute.removerole)) {
			role = msg.guild.roles.get(Bot.Config[msg.guild.id].modules.moderation.unmute.removerole)
		} else {
        	return msg.channel.send(Bot.Helper.Embed("error","Setup Failure","Requested by "+msg.author.tag, msg.author.avatarURL(),"No mute role is set."))
		}
		var user = null
		if (msg.mentions.members.size) {
            if (msg.mentions.members.first()) {
                user = msg.mentions.members.first()
            }
		} else if (msg.guild.members.get(args[0])) {
			user = msg.guild.members.get(args[0])
		} else {
        	return msg.channel.send(Bot.Helper.Embed("error","You need a user to unmute!","Requested by "+msg.author.tag, msg.author.avatarURL()))
		}
		var reason = args.slice(1).toString().replace(/,/g, " ")
		if (user.manageable && user.id != msg.author.id && user.id != Bot.Client.user.id){
			if (reason == "" && !Bot.Config[msg.guild.id].modules.moderation.mute.reasonRequired){
				reason = "No reason provided."
			} else if (reason == "" && Bot.Config[msg.guild.id].modules.moderation.mute.reasonRequired) {
        		return msg.channel.send(Bot.Helper.Embed("error","You need to provide a reason!","Requested by "+msg.author.tag, msg.author.avatarURL()))
			}
			try {
				user.removeRole(role,msg.author.tag+" / "+msg.author.id+" : "+reason)
				msg.channel.send(Bot.Helper.Embed("success","Successfully unmuted "+user.user.tag,"Requested by "+msg.author.tag, msg.author.avatarURL(),reason))
				return user.send(Bot.Helper.Embed("info","You were unmuted in **"+msg.guild.name+"**","Muted by "+msg.author.tag, msg.author.avatarURL(),"Reason: **"+reason+"**"))
			} catch (err) {
        		return msg.channel.send(Bot.Helper.Embed("error","Failed to unmute "+user.user.tag,"Requested by "+msg.author.tag, msg.author.avatarURL(),"For geeks: `"+err+"`"))
			}
		} else {
        	return msg.channel.send(Bot.Helper.Embed("error","I cannot mute "+user.user.tag+"!","Requested by "+msg.author.tag, msg.author.avatarURL()))
		}
	},
};