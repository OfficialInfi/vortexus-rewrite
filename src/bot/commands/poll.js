module.exports = {
	name: "poll",
	module: "utility",
	aliases: ["poll"],
	description: "Create a reaction poll automatically.",
	guildOnly: true,
	async execute(Bot,msg,args) {
		await msg.react("👍")
		await msg.react("👎")
		await msg.react("🤷")
	},
}