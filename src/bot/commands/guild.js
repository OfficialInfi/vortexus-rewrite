module.exports = {
	name: "guild",
	module: "utility",
	aliases: ["guild","guildstats","guildinfo","server","serverstats","serverinfo"],
	description: "View public statistics for the current server",
	guildOnly: true,
	execute(Bot,msg,args) {
		msg.channel.send(Bot.Helper.Embed("info","Public Statistics for "+msg.guild.name,"Requested by "+msg.author.tag, msg.author.avatarURL(),"**Members**: "+msg.guild.memberCount+"\n**Created At**: "+msg.guild.createdAt+"\n**Owner**: "+msg.guild.owner.user.tag+"\n**ID**: "+msg.guild.id))
	},
}