module.exports = {
	name: "work",
	module: "economy",
	aliases: ["work"],
	args: false,
	description: "Work for some money.",
	guildOnly: true,
	cooldown: 300,
	async execute(Bot,msg,args) {
		var user = null
		if (msg.mentions.members.size) {
            if (msg.mentions.members.first()) {
                user = msg.mentions.members.first()
            }
		} else if (msg.guild.members.get(args[0])) {
			user = msg.guild.members.get(args[0])
		} else {
        	user = msg.author
		}
		if (!Bot.Config[msg.guild.id].modules.economy.work.max)
		var balance = await Bot.Helper.Database.get(msg.guild.id,"economy_"+user.id)
		var rand = Math.floor(Math.random() * Number(Bot.Config[msg.guild.id].modules.economy.work.max))
		Bot.Helper.Database.fset(msg.guild.id,"economy_"+msg.author.id,Number(rand))
		msg.channel.send(Bot.Helper.Embed("success","You worked for "+rand+"!","Requested by "+msg.author.tag,msg.author.avatarURL()))
	},
}