module.exports = {
	name: "rank",
	module: "economy",
	aliases: ["rank","levels","lvl","exp"],
	args: false,
	usage: "[user mention/id]",
	description: "View your current rank and exp.",
	guildOnly: true,
	async execute(Bot,msg,args) {
		var user = null
		if (msg.mentions.members.size) {
            if (msg.mentions.members.first()) {
                user = msg.mentions.members.first()
            }
		} else if (msg.guild.members.get(args[0])) {
			user = msg.guild.members.get(args[0])
		} else {
        	user = msg.member
		}
		var level = await Bot.Helper.Database.get(msg.guild.id,"lvl_"+user.id)
		var exp = await Bot.Helper.Database.get(msg.guild.id,"exp_"+user.id)
		if (!level){
			Bot.Helper.Database.fset(msg.guild.id,"lvl_"+msg.author.id,0)
			level = 0
		}
		if (!exp){
			Bot.Helper.Database.fset(msg.guild.id,"exp_"+msg.author.id,0)
			exp = 0
		}
		msg.channel.send(Bot.Helper.Embed("info",user.user.tag+"'s level: "+level,"Requested by "+msg.author.tag,msg.author.avatarURL(),"**Current EXP**: "+exp+"\n**EXP Required**: "+Number(Bot.Config[msg.guild.id].modules.fun.levels.requiredEXP)*Number(level)))
	},
}