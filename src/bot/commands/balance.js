module.exports = {
	name: "balance",
	module: "economy",
	aliases: ["balance","bal","money","bank"],
	args: false,
	usage: "[user mention/id]",
	description: "Check someone's current economy balance.",
	guildOnly: true,
	async execute(Bot,msg,args) {
		var user = null
		if (msg.mentions.members.size) {
            if (msg.mentions.members.first()) {
                user = msg.mentions.members.first()
            }
		} else if (msg.guild.members.get(args[0])) {
			user = msg.guild.members.get(args[0])
		} else {
        	user = msg.member
		}
		var balance = await Bot.Helper.Database.get(msg.guild.id,"economy_"+user.id)
		if (!balance){
			Bot.Helper.Database.fset(msg.guild.id,"economy_"+msg.author.id,0)
			msg.channel.send(Bot.Helper.Embed("info",user.user.tag+"'s balance: 0","Requested by "+msg.author.tag,msg.author.avatarURL()))
		} else if (balance){
			msg.channel.send(Bot.Helper.Embed("info",user.user.tag+"'s balance: "+balance,"Requested by "+msg.author.tag,msg.author.avatarURL()))
		}
	},
}