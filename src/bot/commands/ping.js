module.exports = {
	name: "ping",
	module: "core",
	aliases: ["ping","dong"],
	args: false,
	usage: "none",
	description: "Ping pong!",
	permissions: ["KICK_MEMBERS"],
	cooldown: 5,
	guildOnly: true,
	async execute(Bot,msg,args) {
		msg.channel.send("Pong.")
	},
}