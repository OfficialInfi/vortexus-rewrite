const fs = require("fs");
var log = fs.createWriteStream("./logs/log.txt", {"flags": "a"});

function getTime() {
    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    
    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + ":" + month + ":" + day + " - " + hour + ":" + min + ":" + sec;
}

// functions with _ are verbose

module.exports = {
    log: function(msg) {
        log.write(getTime()+" | [INFO] | "+msg+"\n")
        console.log(getTime()+" | "+"[INFO]"+" | "+msg)
    },
    warn: function(msg) {
        log.write(getTime()+" | [WARN] | "+msg+"\n")
        console.log(getTime()+" | "+"[WARN]"+" | "+msg)
    },
    error: function(msg) {
        log.write(getTime()+" | [ERROR] | "+msg+"\n")
        console.log(getTime()+" | "+"[ERROR]"+" | "+msg)
    },
    _log: function(msg) {
        log.write(getTime()+" | [INFO] | "+msg+"\n")
    },
    _warn: function(msg) {
        log.write(getTime()+" | [WARN] | "+msg+"\n")
    },
    _error: function(msg) {
        log.write(getTime()+" | [ERROR] | "+msg+"\n")
    },
}