const Keyv = require("keyv")
const fs = require("fs")
var guilds = {}

if (!fs.existsSync("database")){
    fs.mkdirSync("database");
}

function getDB(id){
    if (!guilds[id]){
        guilds[id] = new Keyv("sqlite://database/"+id+".sqlite") 
        guilds[id].on("error", err => console.log("Failed to connect to guild database \""+guilds[id]+"\"; Error: ", err));
        return guilds[id]
    } else if (guilds[id]){
        return guilds[id]
    }
}

module.exports = {
    get: async function (id,key){
        return await getDB(id).get(key)
    },
    set: async function (id,key,value){
        if (await getDB(id).get(key)){
            return await getDB(id).get(key)
        } else {
            return await getDB(id).set(key,value)
        }
    },
    fset: async function (id,key,value){
        return await getDB(id).set(key,value)
    },
    add: async function (id,key,value){
        return await getDB(id).set(key,Number(value)+Number(await getDB(id).get(key)))
    },
    del: async function (id,key){
        return await getDB(id).delete(key)
    },
    clr: async function (id){
        return await getDB(id).clear()
    }
}