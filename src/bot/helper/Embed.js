const Discord = require("discord.js")
const types = {
    "success": "#32cd32",
    "error": "#ff0000",
    "info": "#0099ff",
    "star": "#f7ff1a"
}

module.exports = function (type,title,footer,fimage,desc,thumb,url,image){
    if (!types[type]) return
    const Embed = new Discord.MessageEmbed()
        .setTitle(title || "Title")
        .setFooter(footer || null,fimage || null)
        .setDescription(desc || "")
        .setColor(types[type])
        .setThumbnail(thumb || null)
        .setURL(url || null)
        .setImage(image || null)
    return Embed
}