module.exports = {
    bypass: function (Bot,guild,user,channel,msg){
        var bypass = false
        if (!guild) return
        if (!Bot.Config[guild.id]) then
        if (!user) return

        if (Bot.Config[guild.id].modules.moderation.automod.bypass.bot){
            if (user.bot == true){
                bypass = true
            }
        }
        if (Bot.Config[guild.id].modules.moderation.automod.bypass.users){
            for (x of Bot.Config[guild.id].modules.moderation.automod.bypass.users){
                if (x == user.id){
                    bypass = true
                }
            }
        }
        if (Bot.Config[guild.id].modules.moderation.automod.bypass.roles){
            for (x of Bot.Config[guild.id].modules.moderation.automod.bypass.roles){
                if (user.roles.get(x)){
                    bypass = true
                }
            }
        }

        if (Bot.Config[guild.id].modules.moderation.automod.bypass.channels){
            if (!channel) return
            for (x of Bot.Config[guild.id].modules.moderation.automod.bypass.channels){
                if (x == channel.id){
                    bypass = true
                }
            }
        }
        return bypass
    }
}